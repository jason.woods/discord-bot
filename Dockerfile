FROM python:3.9-buster
LABEL maintainer = Jason Woods
RUN apt update
RUN apt-get install -y ffmpeg
RUN useradd bot-user
RUN /usr/local/bin/python3 -m pip install --upgrade pip
COPY --chown=bot-user requirements.txt /var/bot/requirements.txt
WORKDIR /var/bot
RUN pip3 install --user -r requirements.txt
COPY --chown=bot-user . /var/bot/
CMD ["python3","-u", "/var/bot/bot.py"]
