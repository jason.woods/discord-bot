#!/usr/bin/env python
"""
Discord Bot
"""
# ==============================================================================
import logging
import sys
#import time
import traceback
from discord.ext import commands
from config import BOT
from config import DISCORD_TOKEN

# ==============================================================================
# Import all our subclasses
from cogs import chatbot
# ==============================================================================

# Configure logging
logging.basicConfig(level=logging.INFO)


@BOT.event
async def on_ready():
    """Run when bot starts"""
    print("Loading Modules...")

try:
    # Run the bot. Add each sub class as a 'cog'.
    BOT.add_cog(chatbot.main(BOT))
    # Start the bot with the loaded DISCORD_TOKEN
    BOT.run(DISCORD_TOKEN)

except Exception as err:
    print("ERROR - ", err, )
    print(traceback.format_exc())
    sys.exit()
    
