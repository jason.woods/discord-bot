# Config
Rename config.py.example to config.py
Add DISCORD and OPENAI Api Tokens 

# Install dependencies
pip3 install -r requirements.txt

# To run
python3 -m venv bot-env

# Linux
source bot-env/bin/activate

