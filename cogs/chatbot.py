"""
Chatbot Module
"""

import os
import time
import requests
import openai
import logging
import discord
from discord.ext import commands
from config import BOT
from config import OPENAI_TOKEN
openai.api_key = OPENAI_TOKEN
openai.util.logging.getLogger().setLevel(logging.WARNING)
completion = openai.Completion()

start_sequence = "\nAI:"
restart_sequence = "\n\nHuman:"

session_prompt =  "AI: You are talking to an AI, An helpdesk agent." + \
                  "Human: What is your favorite thing to do? \n + \
                  AI: Help you with your questions\n\n"

class main(commands.Cog):
    guilds = BOT.guilds
    last_messages = []
    def __init__(self, bot):
        self.bot = bot
             
    def ask(question, chat_log=None):
      prompt_text = f'{chat_log}{restart_sequence}: {question}{start_sequence}:'
      response = openai.Completion.create(
      model="text-davinci-003",
      prompt= f'{prompt_text}',
      temperature=0.9,
      max_tokens=256,
      top_p=0.8,
      frequency_penalty=0.5,
      presence_penalty=0,
      stop=[" Human:", " AI:"],
      )
      story = response['choices'][0]['text'] 
      return str(story)

    def messages_to_string(message_log):
      result = ""
      for i in message_log:
        result = result + i
      return result
    
    @BOT.listen()
    async def on_message(message):
      
      if message.author == BOT.user or message.author.bot == True:
        return
         
      if BOT.user.mentioned_in(message) or message.channel.type == discord.ChannelType.private:
        question = (message.content)
        async with message.channel.typing():          
          response = main.ask(question, main.messages_to_string(main.last_messages))         
          if len(main.last_messages) == 0:
            main.last_messages.append(session_prompt)
          elif len(main.last_messages) > 10:
            main.last_messages.pop(0)
          main.last_messages.append(f'{restart_sequence}: {question}{start_sequence} {response}')
          print(f'{restart_sequence} {question}' )
          await message.reply(response)
          print(f'{start_sequence}{response} \n\n')
          


      












